"""Command-line interface."""
import click


@click.command()
@click.version_option()
def main() -> None:
    """Deep IVIM."""


if __name__ == "__main__":
    main(prog_name="deepivim")  # pragma: no cover
