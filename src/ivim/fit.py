import argparse
import os
import sys

import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as utils
from dipy.core.gradients import generate_bvecs
from dipy.core.gradients import gradient_table
from dipy.data import get_fnames
from dipy.io.gradients import read_bvals_bvecs
from dipy.reconst.ivim import IvimModel
from dipy.segment.mask import median_otsu
from tqdm import tqdm

def dipy_fit(bvalues, data):

    bvecs = generate_bvecs(len(bvalues))
    gtab = gradient_table(bvalues, bvecs, b0_threshold=0)
    
    print('data.shape (%d, %d, %d, %d)' % data.shape)
    
    z = 10
    b = 0
    
    plt.imshow(data[:, :, z, b].T, origin='lower', cmap='gray',
               interpolation='nearest')
    plt.axhline(y=100)
    plt.axvline(x=170)
    plt.savefig("ivim_data_slice.png")
    plt.close()
    
    ivimmodel = IvimModel(gtab, fit_method='trr')
    
    print(data.shape)
    ivimfit = ivimmodel.fit(np.abs(data))
    
    return ivimfit.model_params

# here I define several neural networks. This one is the vanilla network, with no parameter constraints
class Net(nn.Module):
    def __init__(self, b_values_no0):
        super().__init__()
        self.b_values_no0 = b_values_no0
        self.fc_layers = nn.ModuleList()
        for i in range(3):  # 3 fully connected hidden layers with ELU at the end
            self.fc_layers.extend(
                [nn.Linear(len(b_values_no0), len(b_values_no0)), nn.ELU()]
            )
        self.encoder = nn.Sequential(
            *self.fc_layers, nn.Linear(len(b_values_no0), 4)
        )  # final linear layer

    def forward(self, X):
        params = self.encoder(X)  # estimates of Dp, Dt, Fp and S0
        Dp = params[:, 0].unsqueeze(1)
        Dt = params[:, 1].unsqueeze(1)
        Fp = params[:, 2].unsqueeze(1)
        S0 = params[:, 3].unsqueeze(1)
        # here we give the expected signal decay, given we actually estimated Dp, Dt, Fp and S0
        X = S0 * (
            Fp * torch.exp(-self.b_values_no0 * Dp)
            + (1 - Fp) * torch.exp(-self.b_values_no0 * Dt)
        )
        # output parameters and estimated signal decay
        return X, Dp, Dt, Fp, S0


def infer_ivim(data, bvalues, net):
    # this takes a trained network and predicts parameters from it
    # data --> input data to predict parameters from
    # bvalues --> b-value from data
    # net --> network

    # use GPU if available
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda:0" if use_cuda else "cpu")
    data2 = np.array([x / x[0] for x in data])
    Dp = np.array([])
    Dt = np.array([])
    Fp = np.array([])
    S0 = np.array([])

    net.eval()
    """
    #for i in range(len(data2) // 10000):
    #tell net it is used for evaluation
    net.eval()
    # initialise parameters and data

    if i + 1 == len(data2)//10000:
            inferloader = utils.DataLoader(torch.from_numpy(data2[i*10000::].astype(np.float32)),
                                            batch_size = 2056,
                                            shuffle = False,
                                            drop_last = False)
        else:
            inferloader = utils.DataLoader(torch.from_numpy(data2[i*10000:(i+1)*10000].astype(np.float32)),
                                            batch_size = 2056,
                                            shuffle = False,
                                            drop_last = False)
        """
    inferloader = utils.DataLoader(
        torch.from_numpy(data2.astype(np.float32)),
        batch_size=2056,
        shuffle=False,
        drop_last=False,
    )
    # start infering
    with torch.no_grad():
        for i, X_batch in enumerate(tqdm(inferloader, position=0, leave=True), 0):
            X_batch = X_batch.to(device)
            # here the signal is predicted. Note that we now are interested in the parameters and no longer in thepredicted signal decay.
            _, Dpt, Dtt, Fpt, S0t = net(X_batch)
            S0 = np.append(S0, (S0t.cpu()).numpy())
            Dp = np.append(Dp, (Dpt.cpu()).numpy())
            Dt = np.append(Dt, (Dtt.cpu()).numpy())
            Fp = np.append(Fp, (Fpt.cpu()).numpy())

    if np.mean(Dp) < np.mean(Dt):
        Dp2 = Dt
        Dt = Dp
        Dp = Dp2
        Fp = 1 - Fp

    return np.array([Dp, Dt, Fp, S0])


class hyperparams:
    optim = "adam"  # these are the optimisers implementd. Choices are: 'sgd'; 'sgdr'; 'adagrad' adam
    lr = 0.00025  # this is the learning rate. adam needs order of 0.005; others order of 0.05? sgdr can do 0.5
    lr_Ds = 0.8 * lr  # zet zelfde als lr.
    dropout = 0.3
    run_net = "loss_con"  # these are networks I implemented. One can chose abs_con, hwich used the absolute to constrain parameters. Sig_con which used the sigmoid to constrain parameters, free, which has no parameter constraints, split; which has seperate networks per parameter and tiny, which is a smaller network
    patience = 50  # this is the number of epochs without improvement that the network waits untill determining it found its optimum
    fixS0 = False  # indicates whether to fix S0 in the least squares fit.
    batch_size = 128
    improve = 0.97
    repeats = 50  # this is the number of repeats for simulations


def network_load(path, bvalues):
    # load from a archive the Network
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda:0" if use_cuda else "cpu")

    bvalues = torch.FloatTensor(bvalues[:]).to(device)

    net = Net(bvalues).to(device)
    net.load_state_dict(torch.load(path)) if use_cuda else net.load_state_dict(
        torch.load(path, map_location=torch.device("cpu"))
    )
    net.eval()

    return net



def image_load(path, bvalues):
    # b-values
    bvecs = generate_bvecs(len(bvalues))
    gtab = gradient_table(bvalues, bvecs, b0_threshold=0)

    # .nii load
    img = nib.load(path)
    data = img.get_fdata()

    # Brain extraction
    b0_mask, _mask = median_otsu(
        data,
        median_radius=2,
        numpass=1,
        vol_idx=[0, 1],
    )

    return b0_mask


def save_data(data, path_image):

    img = nib.load(path_image)
    data_img = img.get_fdata()
    output = data.reshape((4, 288, 288, 20))

    return output


def save_archives(output_4d, path_output):

    os.makedirs(path_output, exist_ok=True)

    output_names = ["PseudoDiffusion.nii", "Diffusion.nii", "Fraction.nii", "S0.nii"]

    for i in range(output_4d.shape[0]):
        img = nib.Nifti1Image(output_4d[i, :, :, :], np.eye(output_4d.shape[0]))
        nib.save(img, os.path.join(path_output, output_names[i]))


def save_pngs(output_4d, path_output):

    import matplotlib.pyplot as plt

    # from dipy.core.histeq import histeq
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    sli = output_4d.shape[-1] // 2

    fig, ax = plt.subplots(nrows=2, ncols=2)

    im1 = ax[0, 0].imshow(
        output_4d[0, :, :, sli],
        interpolation="None",
        cmap="viridis",
        vmin=0.0,
        vmax=0.25,
    )

    divider = make_axes_locatable(ax[0, 0])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im1, cax=cax, orientation="vertical")

    im2 = ax[0, 1].imshow(
        output_4d[1, :, :, sli], interpolation="None", cmap="gray", vmin=0.0, vmax=0.01
    )

    divider = make_axes_locatable(ax[0, 1])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im2, cax=cax, orientation="vertical")

    im3 = ax[1, 0].imshow(output_4d[2, :, :, sli], interpolation="None", cmap="viridis")
    divider = make_axes_locatable(ax[1, 0])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im3, cax=cax, orientation="vertical")

    im4 = ax[1, 1].imshow(output_4d[3, :, :, sli], interpolation="None", cmap="viridis")
    divider = make_axes_locatable(ax[1, 1])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im4, cax=cax, orientation="vertical")

    plt.savefig(path_output + "fitting.png")

def main():

    parser = argparse.ArgumentParser(description='Program to fitting IVIM Images')
    parser.add_argument("-m", help="Fitting Method", action="store", type=str, dest="method")
    parser.add_argument("-net", help="Path Network", action="store", type=str, dest="path_net")
    parser.add_argument("-img", help="Path Image", action="store", type=str, dest="path_img")   
    parser.add_argument("-out", help="Path Output", action="store", type=str, dest="path_out")

    args = parser.parse_args()

    os.environ["LRU_CACHE_CAPACITY"] = "1"

    print("Loading Args...")
    if(args.method == None):
        parser.print_help()
        exit(-1)
    else:
        method = args.method
        path_net = args.path_net
        if(args.path_img == None):
            import tkinter as tk
            from tkinter import filedialog

            root = tk.Tk()  
            root.withdraw()

            path_image = filedialog.askopenfilename()
        else:
            path_image = args.path_img
        
        if(args.path_out != None): 
            path_output = args.path_out
        else:
            path_output = 'analysis/'

    print("Loading bvalues")
    bvalues = np.array(
        [0, 4, 8, 16, 30, 60, 120, 250, 500, 1000, 1200, 1400, 1600, 1800, 2000]
    )
    print("Loading data...")
    data = image_load(path_image, bvalues)
 
    if method == 'deeplearn':
        # path = 'pretrained_allloss_con.pt'
        net = network_load(path_net, bvalues)
           
        print("Inference with network...")
        data = data.reshape(np.prod(data.shape[:-1]), data.shape[-1])
        output = infer_ivim(data, bvalues, net)
    
        print(output.shape)
        print(np.nanmean(output[0]))
        print(np.nanmean(output[1]))
        print(np.nanmean(output[2]))
        print(np.nanmean(output[3]))
    
        output_4d = save_data(output, path_image)
    
        save_archives(output_4d, path_output)
    
        save_pngs(output_4d, path_output)
 
    elif method == 'dipy':

        output_4d = dipy_fit(bvalues, data)
        
        save_archives(output_4d, path_output)
        
        save_pngs(output_4d, path_output)

if __name__ == "__main__":

    main()
