# Deep IVIM

[![PyPI](https://img.shields.io/pypi/v/deepivim.svg)][pypi_]
[![Status](https://img.shields.io/pypi/status/deepivim.svg)][status]
[![Python Version](https://img.shields.io/pypi/pyversions/deepivim)][python version]
[![License](https://img.shields.io/pypi/l/deepivim)][license]

[![Read the documentation at https://deepivim.readthedocs.io/](https://img.shields.io/readthedocs/deepivim/latest.svg?label=Read%20the%20Docs)][read the docs]
[![Tests](https://github.com/lcscosta/deepivim/workflows/Tests/badge.svg)][tests]
[![Codecov](https://codecov.io/gh/lcscosta/deepivim/branch/main/graph/badge.svg)][codecov]

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)][pre-commit]
[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)][black]

[pypi_]: https://pypi.org/project/deepivim/
[status]: https://pypi.org/project/deepivim/
[python version]: https://pypi.org/project/deepivim
[read the docs]: https://deepivim.readthedocs.io/
[tests]: https://github.com/lcscosta/deepivim/actions?workflow=Tests
[codecov]: https://app.codecov.io/gh/lcscosta/deepivim
[pre-commit]: https://github.com/pre-commit/pre-commit
[black]: https://github.com/psf/black

## Features

- TODO

## Documentation

- Install poetry, nox and nox-poetry
- Run poetry update
- Run poetry install
- Run poetry run deepivim --args

## Requirements

- TODO

## Installation

You can install _Deep IVIM_ via [pip] from [PyPI]:

```console
$ pip install deepivim
```

## Usage

Please see the [Command-line Reference] for details.

## Contributing

Contributions are very welcome.
To learn more, see the [Contributor Guide].

## License

Distributed under the terms of the [MIT license][license],
_Deep IVIM_ is free and open source software.

## Issues

If you encounter any problems,
please [file an issue] along with a detailed description.

## Credits

This project was generated from [@cjolowicz]'s [Hypermodern Python Cookiecutter] template.

[@cjolowicz]: https://github.com/cjolowicz
[pypi]: https://pypi.org/
[hypermodern python cookiecutter]: https://github.com/cjolowicz/cookiecutter-hypermodern-python
[file an issue]: https://github.com/lcscosta/deepivim/issues
[pip]: https://pip.pypa.io/

<!-- github-only -->

[license]: https://github.com/lcscosta/deepivim/blob/main/LICENSE
[contributor guide]: https://github.com/lcscosta/deepivim/blob/main/CONTRIBUTING.md
[command-line reference]: https://deepivim.readthedocs.io/en/latest/usage.html
